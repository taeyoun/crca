<?php

/**
 * @file
 * Contains \Drupal\devel\Form\ExecutePHP.
 */

namespace Drupal\create_gallery\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;


/**
 * Defines a form that allows privileged users to execute arbitrary PHP code.
 */
class Process extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'create_gallery_form'; // it has be here and return value to call submit()Form
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form = array(
            '#title' => $this->t('Process Create Gallery Module'),
            '#description' => $this->t('Process Create Gallery Module'),
        );
        $form['actions']['#type']='actions';
        $form['actions']['submit']= array(
            '#type' => 'submit',
            '#value' => $this -> t('Save'),
            '#button_type'=>'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
//    public function submitForm(array &$form, FormStateInterface $form_state) {
//        ob_start();
//        $code = $form_state->getValue('code');
//        print eval($code);
//        $_SESSION['devel_execute_code'] = $code;
//        dpm(ob_get_clean());
//    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // save all fil names in an array files[]
        $dir = drupal_get_path('module', 'create_gallery') . '/files/';
        $dh  = opendir($dir);
        $total = 0;
        while ((false !== ($filename = readdir($dh)))) {
            if ($filename !== '.' && $filename !== '..'){
                $files[] = $filename;
                //dsm($filename);
            }
        }
        //$object_array = array();

       // foreach($files as $files_item){
//json file has taxonomy, file name, title, created date,
            $data = file_get_contents($dir . 'gallery.json');

            if($data == true){
                //dsm('success data');
                //create array from json object
                $objects = json_decode($data);
                foreach($objects as $items) {
                    $uris;
//                    $targeted_files;
                    $images=[];
                    //dsm($items);
                    //return;
                    foreach ($items->images as $item) {
                        //dsm($item->title);

                        $file_item = str_replace("sites/default/files/galleries/", "", $item->filepath);
                        // create file object. all images files are located in modules/create_gallery/files/images/
                        // manually copy all files from /sites/default/files/galleries/ (d6 site) to the above path
                        // image file should be in public:// otherwise thumbnail, medium...etc all other types won't be created
                        $uri = file_unmanaged_copy(drupal_get_path('module', 'create_gallery') . '/files/images/' . $file_item, 'public://' . $file_item, FILE_EXISTS_REPLACE);
                        //$uris .= $uri;
                        //dsm($uri);
                        $file = File::Create([
                            'uri' => 'public://' . $file_item,
                            //'uri' => $uris,
                            //'created' => $images['file_created'],
                            'created' => $item->created,
                            //'field_images_title'=> $item->title,
                            //'alt' => $item->title,

                        ]);
                        //$file->get("field)images_title");
                        //dsm($file->getFieldDefinitions());
                        //dsm($file);
                        $file->save();
                        // dsm($file->id());
                        $images[] = array (
                            'target_id' => $file->id(),
                            'path' => 'public://'.$file_item,
                            'alt' => $item->title,
                        );
                    }//dsm($images);

                    $node = Node::create([
                    'type'        => 'gallery',
                    'title'       => $items->name,
                     // set cover images with last item in the images[]
                    'field_image' => [
                        'target_id' => $file->id(),
                        //'path' => '/media/gallery/' . _openchurch_clean_alias('Create Gallery Test with Module'),
                        'path' => 'public://'.$file_item,
                        'alt' => $item->title,
                    ],
                        'field_images' => $images,
                ]);
                    //debug($node->get('field_images'));
                    //dsm($node->getFields('field_images')->getValue());

                    $node->save();
                    //dsm($node->id());return;
                    $total++;

//return;
                }

            }else{
                drupal_set_message(t('Fail to file_get_contents.'));
                return;

            }
        drupal_set_message(t('Total @var Gallery nodes are created.', array('@var' => $total)));

    }
}
