<?php

/**
 * @file
 * Contains \Drupal\devel\Form\ExecutePHP.
 */

namespace Drupal\create_node\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use \Drupal\file\Entity\File;


/**
 * Defines a form that allows privileged users to execute arbitrary PHP code.
 */
class Process extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'example_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form = array(
            '#title' => $this->t('Process Create Node Module'),
            '#description' => $this->t('Process Create Node Module'),
        );
        $form['actions']['#type']='actions';
        $form['actions']['submit']= array(
            '#type' => 'submit',
            '#value' => $this -> t('Save'),
            '#button_type'=>'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $dir = drupal_get_path('module', 'create_node') . '/files/blog/';
        $dh  = opendir($dir);
        while ((false !== ($filename = readdir($dh)))) {
            if ($filename !== '.' && $filename !== '..'){
                $files[] = $filename;
        //        dsm($filename);
            }
        }
        $object_array = array();

    foreach($files as $files_item){

        $data = file_get_contents($dir . $files_item);
        $objects = json_decode($data);
        foreach($objects as $item) {
            array_push($object_array, $item);
        }
    }
        $total=0;
        //dsm($object_array);
        foreach($object_array as $object) {

            $object_array = (array) $object;
            $node = Node::create(array(
                'nid' => NULL,
                'type' => $object_array['type'],
                'title' => $object_array['title'],
                'body' => $object_array['body'],
                'uid' => $object_array['uid'],
                'revision' => mt_rand(0, 1),
                'status' => TRUE,
//                'promote' => mt_rand(0, 1),
                'created' => $object_array['created'],
//                'langcode' => $this->getLangcode($results),
                ));
                $node->save();
            $total++;
        }
        drupal_set_message(t('Total @var nodes are created.', array('@var' => $total)));
    }
}
