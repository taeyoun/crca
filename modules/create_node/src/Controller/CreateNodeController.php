<?php
/**
 * @file
 * Contains \Drupal\create_node\Controller\CreateNodeController.
 */

namespace Drupal\create_node\Controller;

use Drupal\Core\Controller\ControllerBase;

class CreateNodeController extends ControllerBase {
    public function content() {
        return array(
            '#type' => 'markup',
            '#markup' => $this->t('Hello, World!'),
        );
    }

//    public function menuItem() {
//        $item = menu_get_item(current_path());
//        return kdevel_print_object($item);
//    }
}
?>
